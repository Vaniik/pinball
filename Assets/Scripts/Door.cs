﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {
	
	 public List<Rigidbody> balls;
		public bool NoBalls=true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return)&&NoBalls) {
			NoBalls=false;
			AddBalls(3);
			FreezeAllBalls(true);
			RemoveFromList(0);
		}
		
	}

	public void AddBalls(int _balls) {// añadir bolas, tanto crearlas como añadirlas a la lista
		GameObject temp = Resources.Load("Ball") as GameObject;//resources(la carpeta resources, una carpeta especial de unity) y el Load carga el prefab
		
		for(int i = 0; i < _balls; i++) {
			temp.transform.position = new Vector3(13.7f, 0.5f, 15f+i);
			balls.Add(Instantiate(temp).GetComponent<Rigidbody>());//añadimos el rigidbody del componente que queremos para poder freezear
			
		}
	}

	public void FreezeAllBalls(bool freeze) {//freezeamos o unfreezeamos los rigidbodys que tenemos en las listas
		
		foreach(Rigidbody rb in balls) {
			if(freeze)
				rb.constraints = RigidbodyConstraints.FreezeAll;
			else rb.constraints = RigidbodyConstraints.None;
		}
		
	}

	public void RemoveFromList(int _ball) {//quitamos x num de bolas de la lista
		Rigidbody rb=balls[_ball];
		rb.constraints=RigidbodyConstraints.None;
		balls.Remove(balls[_ball]);
	}
	public int getListSize(){
		return balls.Count;
	}
}
