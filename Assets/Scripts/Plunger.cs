﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plunger : MonoBehaviour {

	// Use this for initialization
	SpringJoint spring;
	
    [SerializeField] float maxPosition = 7F;
    [SerializeField] float plungerStrength = 100F;

	[SerializeField] float descSpeed=1;
	void Start () {
		spring=GetComponent<SpringJoint>();
		spring.spring=plungerStrength;
	}
	void Update()
	{
		if(Input.GetKeyDown("space")){
			spring.maxDistance=maxPosition;
		}
		if(Input.GetKeyUp("space")){
			spring.maxDistance=0;
		}
		var z=Input.GetAxis("Plunger")*Time.deltaTime*descSpeed*-1;
		transform.Translate(0,0,z);
	}
}
