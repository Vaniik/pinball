﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfWorld : MonoBehaviour {

	// Use this for initialization
	public bool enter=true;
	public GameObject[] Fangs;
	void Start () {
		
			
			Fangs=GameObject.FindGameObjectsWithTag("Fang");

		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	void OnTriggerEnter(Collider other)
	{
		
		GameObject go = GameObject.Find("Generator");
		GameObject goScore=GameObject.Find("Score");
		GameInstance scoreStats=(GameInstance) goScore.GetComponent(typeof(GameInstance));
		scoreStats.resetFangDown();
		scoreStats.setMultipler(1);
		Door Lose = (Door) go.GetComponent(typeof(Door));
		Debug.Log(Lose.getListSize());
		if(Lose.getListSize()>0){
		Lose.RemoveFromList(0);
		Destroy(other.gameObject);
		}else{
			Lose.NoBalls=true;
			GameObject HS=GameObject.Find("Highscore");
			HighScores HScrypt=(HighScores) HS.GetComponent(typeof(HighScores));
			HScrypt.setHighscore(scoreStats.getScore());
			scoreStats.resetScore();
		}
		foreach(GameObject Fang in Fangs){
			Target targetScript=(Target) Fang.GetComponent(typeof(Target));
			targetScript.setAlive(true);
		}
	}
}
