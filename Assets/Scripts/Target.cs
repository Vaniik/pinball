﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

	// Use this for initialization
	public bool alive=true;

    Rigidbody rb ;
    //Collider myCollider;
    public Vector3 iniPos;
    float YPos;
       
    // Use this for initialization
    void Start () {
        //myCollider=gameObject.GetComponent<Collider>();
        rb=gameObject.GetComponent<Rigidbody>();
        iniPos=transform.position;
       
    }
   
    void Update()
    {
      if(!alive){
          YPos=transform.position.y;
          if(YPos>-4){
              
            transform.Translate(Vector3.down*Time.deltaTime*2);
          }

      }
    }
    void OnCollisionEnter(Collision other)
    {   GameObject goScore=GameObject.Find("Score");
		GameInstance scoreStats=(GameInstance) goScore.GetComponent(typeof(GameInstance));
        if(alive){
            alive=false;
            scoreStats.addFangDown();
        }
		scoreStats.AddScore(200);
    }
    public void setAlive(bool response){
        alive=response;
        if(response){
            transform.position=iniPos;
        }
    }
}
