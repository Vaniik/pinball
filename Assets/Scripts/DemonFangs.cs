﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonFangs : MonoBehaviour {

	// Use this for initialization
	[SerializeField] float bumperForce=0F;
	private GameObject Ball;
	void Start () {
		Ball=GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionStay(Collision collider)
	{
		if(collider.gameObject.tag=="Ball"){
			foreach(var contact in collider.contacts){
				contact.otherCollider.GetComponent<Rigidbody>().AddForce(contact.normal*bumperForce*-1,ForceMode.Impulse);
			}
			GameObject goScore=GameObject.Find("Score");
			GameInstance scoreStats=(GameInstance) goScore.GetComponent(typeof(GameInstance));
			scoreStats.AddScore(50);
		}
	}
}
