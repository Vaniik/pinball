﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInstance : MonoBehaviour {

	private int Score;
	private int multipler;
	public int FangsDown;
	public  GameObject[] Fangs;
	TextMesh myText;
	// Use this for initialization
	void Start () {
		FangsDown=0;
		Score=0;
		multipler=1;
		myText=GetComponent<TextMesh>();
		myText.text=Score.ToString();
		Fangs=GameObject.FindGameObjectsWithTag("Fang");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void AddScore(int points){
		Score+=points*multipler;
		myText.text=Score.ToString();
	}
	public void resetScore(){
		Score=0;
		multipler=1;
		myText.text=Score.ToString();
		
	}
	public void setMultipler(int mult){
		multipler=mult;
	}
	public void addFangDown(){
		FangsDown+=1;
		
		if(FangsDown==Fangs.Length){
			setMultipler(3);
		}
	}
	public void resetFangDown(){
		FangsDown=0;
	}
	public int getScore(){
		return Score;
	}
}
