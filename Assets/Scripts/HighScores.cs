﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScores : MonoBehaviour {
	int Score;
	// Use this for initialization
	TextMesh myText;
	void Start () {
		Score=0;
		myText=GetComponent<TextMesh>();
		myText.text=Score.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void setHighscore(int HighMark){
		Score=HighMark;
		myText.text=Score.ToString();
	}
}
